import 'package:flutter/material.dart';
import 'screens/Login.dart';

void main() {
  appStart(const theApp());
}

class theApp extends StatelessWidget {
  const theApp({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.yellow,
      ),
      home: const LogInScreen(),
    );
    
  }
}
