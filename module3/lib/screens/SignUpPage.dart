import 'package:flutter/material.dart';
import 'package:module3/screens/Login.dart';
import 'package:module3/Theme.dart';
import 'package:module3/widgets/CheckBox.dart';
import 'package:module3/widgets/Button.dart';
import 'package:module3/widgets/SignUp.dart';

class SignUpScreen extends StatelessWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 70,
            ),
            Padding(
              padding: defaultPadding,
              child: Text(
                'Create Account',
                style: titleText,
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            Padding(
                padding: defaultPadding,
                child: Row(
                  children: [
                    Text(
                      'Already have an account?',
                      style: subTitle,
                    ),
                    const SizedBox(
                      width: 7,
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const LogInScreen()));
                      },
                      child: Text(
                        'LogIn',
                        style: textButton.copyWith(
                          decoration: TextDecoration.underline,
                          decorationThickness: 1,
                        ),
                      ),
                    ),
                  ],
                )),
            const SizedBox(
              height: 15,
            ),
            const Padding(
              padding: defaultPadding,
              child: SignUpForm(),
            ),
            const SizedBox(
              height: 300,
            ),
            const Padding(
              padding: defaultPadding,
              child: CheckBox('Agree to Terms and Conditions.'),
            ),
            const SizedBox(
              height: 30,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const LogInScreen(),
                  ),
                );
              },
              child: const Padding(
                padding: defaultPadding,
                child: PrimaryButton(buttonText: 'SignUp'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
